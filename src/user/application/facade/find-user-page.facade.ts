import { Injectable } from '@nestjs/common';
import { BaseFacade } from '@src/@shared/application';
import { FindUserPageView } from '@src/@shared/common/generated';
import { FindUserPageInput } from '@src/user/application/facade/find-user-page.input';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import { UserPageDto } from '@src/user/domain/query/user.dto';

@Injectable()
export class FindUserPageFacade extends BaseFacade<FindUserPageView> {
  constructor(private readonly userQueryService: UserQueryService) {
    super();
  }

  override async execute(input: FindUserPageInput): Promise<FindUserPageView> {
    const { names, roleIds, size, index } = input;

    const userPageDto = await this.userQueryService.findUserPage({
      filter: { names, roleIds },
      order: [['id', 'DESC']],
      pagination: { size, index },
    });

    return this.generateView(userPageDto, size, index);
  }

  protected override generateView(userPageDto: UserPageDto, size: number, index: number): FindUserPageView {
    const { users, totalElement, totalPage } = userPageDto;

    return {
      content: users.map((user) => ({
        ...user,
        userRoles: [],
      })),
      pageInfo: { totalElement, totalPage, size, index },
    };
  }
}
