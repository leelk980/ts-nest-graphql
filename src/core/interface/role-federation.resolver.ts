import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { RoleField, UserRoleField } from '@src/@shared/common/generated';
import { RoleLoader } from '@src/core/application/loader/role.loader';

@Resolver()
export class RoleFederationResolver {
  constructor(private readonly roleLoader: RoleLoader) {}

  @ResolveField()
  @Resolver('UserRoleField')
  async role(@Parent() userRoleField: UserRoleField): Promise<RoleField> {
    return this.roleLoader.load(userRoleField.roleId);
  }
}
