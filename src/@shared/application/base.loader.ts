import DataLoader from 'dataloader';

export abstract class BaseLoader<TKey, TField> {
  protected abstract dataLoader: DataLoader<TKey, TField>;

  /** should be arrow function */
  protected abstract batchLoadFunction(...args: any[]): Promise<TField[]>;
}
