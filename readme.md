# 소개

NestJS + GraphQL + TypeORM로 구성된 보일러 프로젝트

### 폴더 구조

1. environment
    1. 환경변수 .env 파일 모아둔 폴더
2. schema
    1. graphql 스키마를 모아둔 폴더
    2. npm run script:schema를 이용해 typescript generation
3. src
    1. @client
        1. 다른 domain 모듈을 호출할 때 사용할 클라이언트 인터페이스 정의  
           ex) CoreClient
        2. 각 domain 모듈에서는 자기의 client를 구현하고 exports에 등록  
           ex) CoreInternal
        3. application layer에서 client를 사용  
           ex) CreateUserSaga의 CoreClient
    2. @config
        1. app.module.ts에서 쓰이는 설정들을 모아둔 폴더
            1. extension: 확장함수
            2. scalar: custom graphql 타입
            3. index.ts: 기타 등등
    3. @shared
        1. domain 모듈을 만들때 필요한 base.*.ts 모음
    4. domain 모듈 (core, user...)
        1. 비즈니스 domain 모듈

### **도메인 모듈

#### 레이어

1. interface
    1. request를 받아내는 출입구
    2. query, mutation, (field), federation
        1. 사용하는 타입 = Field, View, Input
2. application
    1. 도메인을 조합하는 로직을 수행
    2. facade, saga, loader
        1. 사용하는 타입 = Field, View, Input
        2. Input만 generation된 타입을 바로 쓰지 않고 validation을 위해 class로 구현
3. domain
    1. 도메인 내의 비즈니스 로직을 수행
    2. queryService, commandService
        1. 사용하는 타입 = Vo, Dto, Param
    3. repository, adapter
        1. 사용하는 타입 = Entity + Entity 타입 유틸리티
    4. entity
4. infrastructure
    1. domain에서 정의한 추상 클래스를 구현
5. common
    1. 위의 4가지 이외의 것들 config, util, enum 등등

#### 좋은점

1. @Transaction 데코레이터
2. Dataloader로 N+1 해결
3. 도메인에서 정의한 abstarct class로 infra에 있는 구현체를 DI
    1. base-repository-impl
4. 테스트 모듈
    1. Database 연결O - database-test.factory.ts
    2. Database 연결X - mock-test.factory.ts

#### 개선할점

1. GraphQL E2E 테스트 - supertest builder
2. Enum
3. 구현체를 Impl이라고 네이밍하지 않기
