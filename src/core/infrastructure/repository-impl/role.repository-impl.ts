import { BaseRepositoryImpl } from '@src/@shared/infrastructure';
import { RoleEntity } from '@src/core/domain/entity/role.entity';
import { RoleRepository } from '@src/core/domain/repository/role.repository';

export class RoleRepositoryImpl extends BaseRepositoryImpl<RoleEntity> implements RoleRepository {}
