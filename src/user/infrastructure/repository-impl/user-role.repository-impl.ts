import { BaseRepositoryImpl } from '@src/@shared/infrastructure';
import { UserRoleEntity } from '@src/user/domain/entity/user-role.entity';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';

export class UserRoleRepositoryImpl extends BaseRepositoryImpl<UserRoleEntity> implements UserRoleRepository {
  async findManyByUserIds(userIds: number[]): Promise<Omit<UserRoleEntity, 'user'>[]> {
    return this.createQueryBuilder('userRole').where('userRole.userId in (:...userIds)', { userIds }).getMany();
  }
}
