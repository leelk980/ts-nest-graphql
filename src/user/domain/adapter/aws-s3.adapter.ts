import { Adapter, BaseAdapter } from '@src/@shared/application';
import { AwsS3AdapterImpl } from '@src/user/infrastructure/adapter-impl/aws-s3.adapter-impl';

@Adapter(AwsS3AdapterImpl)
export abstract class AwsS3Adapter extends BaseAdapter {
  abstract getPreSignedUrl(): string;
}
