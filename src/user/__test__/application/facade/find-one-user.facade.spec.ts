import { MockTestFactory } from '@src/@shared/__test__';
import { FindOneUserFacade } from '@src/user/application/facade/find-one-user.facade';
import { UserEntity } from '@src/user/domain/entity/user.entity';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import { when } from 'ts-mockito';

describe('FindOneUserFacade', () => {
  let findOneUserFacade: FindOneUserFacade;
  let userQueryService: UserQueryService;

  MockTestFactory.builder<typeof FindOneUserFacade>()
    .setInjector(FindOneUserFacade)
    .setMocks(UserQueryService)
    .setInitCallback((injector, [mock1]) => {
      findOneUserFacade = injector;
      userQueryService = mock1;
    })
    .build();

  describe('execute', () => {
    test('success', async () => {
      // given
      const userEntity: UserEntity = {
        id: 123,
        email: 'test@test.com',
        name: 'test',
        summary: 'test',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      when(userQueryService.findOneUserById(123)).thenResolve({ user: userEntity });

      // when
      const result = await findOneUserFacade.execute({ id: 123 });

      // then
      expect(result.content.id).toEqual(123);
      expect(result.content.email).toEqual('test@test.com');
      expect(result.content.name).toEqual('test');
      expect(result.content.summary).toEqual('test');
    });
  });
});
