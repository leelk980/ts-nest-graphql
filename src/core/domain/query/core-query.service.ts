import { Injectable } from '@nestjs/common';
import { RolesDto } from '@src/core/domain/query/role.dto';
import { RoleRepository } from '@src/core/domain/repository/role.repository';

@Injectable()
export class CoreQueryService {
  constructor(private readonly roleRepository: RoleRepository) {}

  async findAllRole(): Promise<RolesDto> {
    const roles = await this.roleRepository.findAll();

    return { roles };
  }

  async findManyRoleByIds(ids: number[]): Promise<RolesDto> {
    const roles = await this.roleRepository.findManyByIds(ids);

    return { roles };
  }
}
