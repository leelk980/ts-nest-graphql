import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { IMutation } from '@src/@shared/common/generated';
import { CreateUserInput } from '@src/user/application/saga/create-user.input';
import { CreateUserSaga } from '@src/user/application/saga/create-user.saga';
import { DeleteUserInput } from '@src/user/application/saga/delete-user.input';
import { DeleteUserSaga } from '@src/user/application/saga/delete-user.saga';
import { UpdateUserInput } from '@src/user/application/saga/update-user.input';
import { UpdateUserSaga } from '@src/user/application/saga/update-user.saga';

type IUserMutation = Pick<IMutation, 'createUser' | 'updateUser' | 'deleteUser'>;

@Resolver('Mutation')
export class UserMutationResolver implements IUserMutation {
  constructor(
    private readonly createUserSaga: CreateUserSaga,
    private readonly updateUserSaga: UpdateUserSaga,
    private readonly deleteUserSaga: DeleteUserSaga,
  ) {}

  @Mutation()
  async createUser(@Args('input') input: CreateUserInput): Promise<number> {
    return this.createUserSaga.execute(input);
  }

  @Mutation()
  async updateUser(@Args('input') input: UpdateUserInput): Promise<number> {
    return this.updateUserSaga.execute(input);
  }

  @Mutation()
  async deleteUser(@Args('input') input: DeleteUserInput): Promise<number> {
    return this.deleteUserSaga.execute(input);
  }
}
