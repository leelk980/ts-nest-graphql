import { BaseRepository, Repository } from '@src/@shared/domain';
import { UserRoleEntity } from '@src/user/domain/entity/user-role.entity';
import { UserRoleRepositoryImpl } from '@src/user/infrastructure/repository-impl/user-role.repository-impl';

@Repository(UserRoleEntity, UserRoleRepositoryImpl)
export abstract class UserRoleRepository extends BaseRepository<UserRoleEntity, 'user'> {
  abstract findManyByUserIds(userIds: number[]): Promise<Omit<UserRoleEntity, 'user'>[]>;
}
