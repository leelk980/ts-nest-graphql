type UserRoleVo = {
  id: number;

  userId: number;

  roleId: number;
};

export type UserRolesDto = {
  userRoles: UserRoleVo[];
};
