import { Injectable } from '@nestjs/common';
import { RoleTitleEnum } from '@src/core/common/enum/RoleTitleEnum';
import { RoleRepository } from '@src/core/domain/repository/role.repository';

@Injectable()
export class CoreCommandService {
  constructor(private readonly roleRepository: RoleRepository) {}

  async insertSeedData() {
    await this.insertRoleSeedData();
  }

  private async insertRoleSeedData() {
    const roles = await this.roleRepository.findAll();
    if (roles.length === 0) {
      const toCreate = this.roleRepository.createMany([
        { title: RoleTitleEnum.NORMAL },
        { title: RoleTitleEnum.MEMBER },
        { title: RoleTitleEnum.ADMIN },
      ]);

      await this.roleRepository.saveMany(toCreate);
    }
  }
}
