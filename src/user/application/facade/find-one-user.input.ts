import { FindOneUserInput as IFindOneUserInput } from '@src/@shared/common/generated';
import { IsNumber } from 'class-validator';

export class FindOneUserInput implements IFindOneUserInput {
  @IsNumber()
  id!: number;
}
