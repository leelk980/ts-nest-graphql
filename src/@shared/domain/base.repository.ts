import { BaseEntity } from '@src/@shared/domain';
import { CreateEntity } from '@src/@shared/domain/type';

/**
 * @description
 * T = Entity
 * K = Relation properties
 * */
export abstract class BaseRepository<T extends BaseEntity, K extends keyof Omit<T, keyof BaseEntity> = never> {
  abstract createOne(entity: CreateEntity<T, K>): T;

  abstract createMany(entities: CreateEntity<T, K>[]): T[];

  abstract saveOne(entity: T): Promise<number>;

  abstract saveMany(entities: T[]): Promise<number[]>;

  abstract updateOne(entity: T): Promise<number>;

  // TODO: updateBatch()

  abstract deleteOne(entity: T): Promise<number>;

  abstract deleteMany(entities: T[]): Promise<number[]>;

  abstract deleteAll(): Promise<void>;

  /** Join 걸지 않으므로 K를 Omit */
  abstract findAll(): Promise<Omit<T, K>[]>;

  abstract findOneById(id: number): Promise<Omit<T, K> | null>;

  abstract findManyByIds(ids: number[]): Promise<Omit<T, K>[]>;
}
