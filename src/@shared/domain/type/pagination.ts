export type Pagination = {
  size: number;
  index: number;
};
