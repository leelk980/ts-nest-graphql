import { BaseRepository, Repository } from '@src/@shared/domain';
import { JoinEntity, Order, Pagination } from '@src/@shared/domain/type';
import { UserEntity } from '@src/user/domain/entity/user.entity';
import { UserRepositoryImpl } from '@src/user/infrastructure/repository-impl/user.repository-impl';

@Repository(UserEntity, UserRepositoryImpl)
export abstract class UserRepository extends BaseRepository<UserEntity, 'userRoles'> {
  abstract findOneByIdWithRoles(id: number): Promise<JoinEntity<UserEntity, 'userRoles'> | null>;

  abstract findPage(
    filter: { names: string[]; roleIds: number[] },
    order: Order,
    pagination: Pagination,
  ): Promise<{ users: Omit<UserEntity, 'userRoles'>[]; totalElement: number; totalPage: number }>;
}
