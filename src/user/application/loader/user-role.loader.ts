import { Injectable } from '@nestjs/common';
import { BaseLoader } from '@src/@shared/application';
import { UserRoleField } from '@src/@shared/common/generated';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import * as DataLoader from 'dataloader';

type UserId = number;

@Injectable()
export class UserRoleLoader extends BaseLoader<UserId, UserRoleField[]> {
  constructor(private readonly userQueryService: UserQueryService) {
    super();
  }

  async load(userId: UserId) {
    return this.dataLoader.load(userId);
  }

  protected override batchLoadFunction = async (userIds: readonly UserId[]) => {
    const userRolesDto = await this.userQueryService.findManyUserRoleByUserIds(userIds as UserId[]);

    const userRolesMap: Record<UserId, UserRoleField[]> = userRolesDto.userRoles.groupBy('userId', userIds as UserId[]);

    return userIds.map((userId) => userRolesMap[userId]);
  };

  protected override dataLoader = new DataLoader(this.batchLoadFunction);
}
