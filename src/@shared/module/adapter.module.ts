import { Provider } from '@nestjs/common';
import { ADAPTER_IMPL, BaseAdapter } from '@src/@shared/application';

export class AdapterModule {
  public static register(...adapters: typeof BaseAdapter[]) {
    const providers: Provider[] = [];

    for (const adapter of adapters) {
      const adapterImpl = Reflect.getMetadata(ADAPTER_IMPL, adapter);

      if (!adapterImpl) {
        continue;
      }

      providers.push({
        provide: adapter,
        useClass: adapterImpl,
      });
    }

    return providers;
  }
}
