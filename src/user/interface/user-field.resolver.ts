import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { UserField, UserRoleField } from '@src/@shared/common/generated';
import { UserRoleLoader } from '@src/user/application/loader/user-role.loader';

@Resolver('UserField')
export class UserFieldResolver {
  constructor(private readonly userRoleLoader: UserRoleLoader) {}

  @ResolveField()
  async userRoles(@Parent() userField: UserField): Promise<UserRoleField[]> {
    return this.userRoleLoader.load(userField.id);
  }
}
