import { DateScalar } from '@src/@config/scalar/date.scalar';

export const GraphQLScalars = [DateScalar];
