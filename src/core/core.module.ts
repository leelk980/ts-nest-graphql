import { Module } from '@nestjs/common';
import { InternalModule, RepositoryModule } from '@src/@shared/module';
import { FindAllRoleFacade } from '@src/core/application/facade/find-all-role.facade';
import { RoleLoader } from '@src/core/application/loader/role.loader';
import { CoreCommandService } from '@src/core/domain/command/core-command.service';
import { CoreQueryService } from '@src/core/domain/query/core-query.service';
import { RoleRepository } from '@src/core/domain/repository/role.repository';
import { CoreQueryResolver } from '@src/core/interface/core-query.resolver';
import { CoreInternal } from '@src/core/interface/core.internal';
import { RoleFederationResolver } from '@src/core/interface/role-federation.resolver';

// interface
const internals = InternalModule.register(CoreInternal);
const resolvers = [CoreQueryResolver, RoleFederationResolver];

// application
const facades = [FindAllRoleFacade];
const loaders = [RoleLoader];
const sagas: any[] = [];

// domain
const domainServices = [CoreQueryService, CoreCommandService];

// infrastructure
const adapters: any[] = [];
const repositories = RepositoryModule.register(RoleRepository);

@Module({
  providers: [
    ...internals,
    ...resolvers,
    ...facades,
    ...loaders,
    ...sagas,
    ...adapters,
    ...domainServices,
    ...repositories,
  ],
  exports: [...internals],
})
export class CoreModule {}
