import { MockTestFactory } from '@src/@shared/__test__';
import { UserEntity } from '@src/user/domain/entity/user.entity';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';
import { UserRepository } from '@src/user/domain/repository/user.repository';
import { when } from 'ts-mockito';

describe('UserQueryService', () => {
  let userQueryService: UserQueryService;
  let userRepository: UserRepository;
  let userRoleRepo: UserRoleRepository;

  MockTestFactory.builder<typeof UserQueryService>()
    .setInjector(UserQueryService)
    .setMocks(UserRepository, UserRoleRepository)
    .setInitCallback((injector, [mock1]) => {
      userQueryService = injector;
      userRepository = mock1;
    })
    .build();

  describe('findOneUserById', () => {
    it('user가 존재하면 userDto를 리턴한다', async () => {
      // given
      const userEntity: UserEntity = {
        id: 123,
        email: 'test@test.com',
        name: 'test',
        summary: 'test',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      when(userRepository.findOneById(123)).thenResolve(userEntity);

      // when
      const result = await userQueryService.findOneUserById(123);

      // then
      expect(result).toEqual<typeof result>({
        user: userEntity,
      });
    });

    it('user가 존재하지 않으면, NotFoundException을 던진다', async () => {
      // given
      when(userRepository.findOneById(456)).thenResolve(null);

      // when
      const result = userQueryService.findOneUserById(456);

      // then
      await expect(result).rejects.toThrowError();
    });
  });
});
