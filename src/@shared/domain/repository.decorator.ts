import { SetMetadata } from '@nestjs/common';
import { BaseEntity } from '@src/@shared/domain';
import { BaseRepositoryImpl } from '@src/@shared/infrastructure';

export const REPOSITORY_IMPL = 'REPOSITORY_IMPL';

/**
 * @Description
 * - typeorm 0.3.0 버전부터 customRepository 기능이 deprecated 되어서 직접 구현함
 * - repository에 entity와 구현체를 명시해야함
 * */
export function Repository<T extends BaseEntity>(
  entity: new (...args: any[]) => T,
  implement: typeof BaseRepositoryImpl<T>,
): ClassDecorator {
  return SetMetadata(REPOSITORY_IMPL, [entity, implement]);
}
