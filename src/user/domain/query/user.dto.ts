type UserVo = {
  id: number;

  name: string;

  email: string;

  summary: string | null;

  createdAt: Date;
};

export type UserPageDto = {
  users: UserVo[];

  totalElement: number;

  totalPage: number;
};

export type UsersDto = {
  users: UserVo[];
};

export type UserDto = {
  user: UserVo;
};
