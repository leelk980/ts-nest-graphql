import { BadRequestException } from '@nestjs/common';
import { CoreClient, RoleRo, RoleWhere } from '@src/@client';
import { Order } from '@src/@shared/domain/type';
import { BaseInternal, Internal } from '@src/@shared/interface';
import { CoreQueryService } from '@src/core/domain/query/core-query.service';

@Internal(CoreClient)
export class CoreInternal extends BaseInternal implements CoreClient {
  constructor(private readonly coreQueryService: CoreQueryService) {
    super();
  }

  async $findManyRole<K extends keyof RoleWhere>(options: {
    where: PickEntry<RoleWhere, K>;
    order?: Order<'id' | 'title'>;
  }): Promise<RoleRo[]> {
    const { where } = options;

    if (this.checkRoleWhere('all', where)) {
      return (await this.coreQueryService.findAllRole()).roles;
    }

    if (this.checkRoleWhere('ids', where)) {
      return (await this.coreQueryService.findManyRoleByIds(where[1])).roles;
    }

    if (this.checkRoleWhere('titles', where)) {
      // return (await this.coreQueryService.findManyRoleByIds(where[1])).roles;
    }

    throw new BadRequestException(`[$findManyRole] bad options: ${JSON.stringify(options)}`);
  }

  private checkRoleWhere<K extends keyof RoleWhere>(key: K, where: [any, any]): where is [K, RoleWhere[K]] {
    return where[0] === key;
  }
}
