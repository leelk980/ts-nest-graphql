import { Injectable } from '@nestjs/common';
import { BaseLoader } from '@src/@shared/application';
import { RoleField } from '@src/@shared/common/generated';
import { CoreQueryService } from '@src/core/domain/query/core-query.service';
import * as DataLoader from 'dataloader';

type RoleId = number;

@Injectable()
export class RoleLoader extends BaseLoader<RoleId, RoleField> {
  constructor(private readonly coreQueryService: CoreQueryService) {
    super();
  }

  async load(roleId: RoleId) {
    return this.dataLoader.load(roleId);
  }

  protected override batchLoadFunction = async (roleIds: readonly RoleId[]) => {
    const rolesDto = await this.coreQueryService.findManyRoleByIds(roleIds as RoleId[]);

    const roleMap: Record<RoleId, RoleField> = rolesDto.roles.associateBy('id');

    return roleIds.map((roleId) => roleMap[roleId]);
  };

  protected override dataLoader = new DataLoader(this.batchLoadFunction);
}
