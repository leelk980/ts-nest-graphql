import { BaseEntity } from '@src/@shared/domain';
import { UserRoleEntity } from '@src/user/domain/entity/user-role.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity('user')
export class UserEntity extends BaseEntity {
  @Column({ type: 'varchar' })
  name!: string;

  @Column({ type: 'varchar', unique: true })
  email!: string;

  @Column({ type: 'varchar', nullable: true })
  summary!: string | null;

  /** Relations */
  @OneToMany(() => UserRoleEntity, (userRole) => userRole.user)
  userRoles?: UserRoleEntity[];
}
