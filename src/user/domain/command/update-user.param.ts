export type UpdateAggregateParam = {
  id: number;
  name?: string;
  email?: string;
  summary?: string | null;
  userRoles?: {
    roleId: number;
  }[];
};
