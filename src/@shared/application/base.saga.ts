import { OnModuleInit } from '@nestjs/common';
import { NODE_ENV } from '@src/@config';
import { getApplication } from '@src/app.holder';
import { ClsService } from 'nestjs-cls';
import { DataSource } from 'typeorm';

/**
 * @description
 * 트랜잭션 매니저
 * */
export abstract class BaseSaga<T> implements OnModuleInit {
  dataSource?: DataSource;
  clsService?: ClsService;

  // app.listen() 해야 실행됨
  onModuleInit() {
    const application = getApplication();

    this.dataSource = application.get(DataSource);
    this.clsService = application.get(ClsService);
  }

  abstract execute(input: T): any;
}

/**
 * @description
 * 트랜잭션 사용할 메소드 명시
 * - Base Saga의 하위 메소드에만 붙여야함
 * */
export const Transaction = () => {
  return function (
    _target: any,
    _name: string,
    descriptor: TypedPropertyDescriptor<any>,
  ): TypedPropertyDescriptor<any> {
    const originalMethod = descriptor.value;

    return {
      configurable: true,
      enumerable: false,
      get() {
        return async (...args: any[]) => {
          const that = this as BaseSaga<any>;
          if (!that.dataSource || !that.clsService) {
            console.warn(`[${NODE_ENV}]: ${that.constructor.name} 트랜잭션 작동 안함`);
            return originalMethod.call(that, ...args);
          }

          const queryRunner = that.dataSource.createQueryRunner();
          await queryRunner.connect();

          that.clsService.set('QUERY_RUNNER', queryRunner); // TODO: enum으로 관리

          let result: any;
          let error: any;
          try {
            await queryRunner.startTransaction();

            result = await originalMethod.call(that, ...args);

            await queryRunner.commitTransaction();
          } catch (err) {
            error = err;
            await queryRunner.rollbackTransaction();
          } finally {
            await queryRunner.release();
          }

          if (error) throw error;
          return result;
        };
      },
    };
  };
};
