import { Injectable } from '@nestjs/common';
import { BaseSaga, Transaction } from '@src/@shared/application';
import { DeleteUserInput } from '@src/user/application/saga/delete-user.input';
import { UserCommandService } from '@src/user/domain/command/user-command.service';

@Injectable()
export class DeleteUserSaga extends BaseSaga<DeleteUserInput> {
  constructor(private readonly userCommandService: UserCommandService) {
    super();
  }

  @Transaction()
  override async execute(input: DeleteUserInput) {
    const { id } = input;

    return this.userCommandService.deleteAggregate(id);
  }
}
