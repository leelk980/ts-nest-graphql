export type CreateAggregateParam = {
  name: string;
  email: string;
  summary: string | null;
  userRoles: {
    roleId: number;
  }[];
};
