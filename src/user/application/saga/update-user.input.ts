import { UpdateUserInput as IUpdateUserInput } from '@src/@shared/common/generated';
import { IsArray, IsEmail, IsNumber, IsString } from 'class-validator';

export class UpdateUserInput implements IUpdateUserInput {
  @IsNumber()
  id!: number;

  @IsEmail()
  email!: string;

  @IsString()
  name!: string;

  @IsString()
  summary?: Nullable<string>;

  @IsArray()
  @IsNumber({}, { each: true })
  roleIds!: number[];
}
