import { Injectable } from '@nestjs/common';
import { BaseFacade } from '@src/@shared/application';
import { FindAllRoleView } from '@src/@shared/common/generated';
import { CoreQueryService } from '@src/core/domain/query/core-query.service';
import { RolesDto } from '@src/core/domain/query/role.dto';

@Injectable()
export class FindAllRoleFacade extends BaseFacade<FindAllRoleView> {
  constructor(private readonly coreQueryService: CoreQueryService) {
    super();
  }

  override async execute(): Promise<FindAllRoleView> {
    const rolesDto = await this.coreQueryService.findAllRole();

    return this.generateView(rolesDto);
  }

  protected generateView(rolesDto: RolesDto): FindAllRoleView {
    const { roles } = rolesDto;

    return {
      content: roles,
      count: roles.length,
    };
  }
}
