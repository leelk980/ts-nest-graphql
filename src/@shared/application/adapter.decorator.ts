import { SetMetadata } from '@nestjs/common';
import { BaseAdapterImpl } from '@src/@shared/infrastructure';

export const ADAPTER_IMPL = 'ADAPTER_IMPL';

export function Adapter<T extends BaseAdapterImpl>(implement: new (...args: any) => T): ClassDecorator {
  return SetMetadata(ADAPTER_IMPL, implement);
}
