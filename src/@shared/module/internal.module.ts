import { Provider } from '@nestjs/common';
import { BaseInternal, INTERNAL } from '@src/@shared/interface';

export class InternalModule {
  public static register(...internals: (new (...args: any[]) => BaseInternal)[]) {
    const providers: Provider[] = [];

    for (const i of internals) {
      const client = Reflect.getMetadata(INTERNAL, i);

      if (!client) {
        continue;
      }

      providers.push({
        provide: client,
        useClass: i,
      });
    }

    return providers;
  }
}
