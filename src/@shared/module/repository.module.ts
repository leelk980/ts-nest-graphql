import { Provider } from '@nestjs/common';
import { getDataSourceToken } from '@nestjs/typeorm';
import { BaseRepository, REPOSITORY_IMPL } from '@src/@shared/domain';
import { BaseRepositoryImpl } from '@src/@shared/infrastructure';
import { DataSource } from 'typeorm';

/** @description
 *  repository DI할때 사용할 유틸성 모듈
 *  - @Repository 데코레이터로 등록한 구현체를 넣어줌
 * */
export class RepositoryModule {
  public static register(...repositories: typeof BaseRepository<any>[]) {
    const providers: Provider[] = [];

    for (const repo of repositories) {
      const [entity, repoImpl] = Reflect.getMetadata(REPOSITORY_IMPL, repo);

      if (!entity) {
        continue;
      }

      providers.push({
        inject: [getDataSourceToken()],
        provide: repo,
        useFactory: (dataSource: DataSource): BaseRepositoryImpl<any> => {
          const baseRepository = dataSource.getRepository<any>(entity);

          return new repoImpl(baseRepository.target, baseRepository.manager, baseRepository.queryRunner);
        },
      });
    }

    return providers;
  }
}
