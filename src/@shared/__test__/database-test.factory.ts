import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfig } from '@src/@config';
import { RepositoryModule } from '@src/@shared/module';
import { DataSource } from 'typeorm';

jest.setTimeout(30000);

type TRepository<T extends new (...args: any[]) => any> = abstract new (...args: any) => UnwrapArray<
  ConstructorParameters<T>
>;

/** @description
 * This factory is for domain layer test
 * 트랜잭션 작동하지 않음
 * */
export class DatabaseTestFactory<T extends new (...args: any[]) => any> {
  private service!: T;
  private repositories!: TRepository<T>[];
  private initCallback!: (application: INestApplication) => void;
  private cleanupCallback!: (application: INestApplication) => Promise<void>;
  private application!: INestApplication;

  private constructor() {
    /***/
  }

  static builder<T extends new (...args: any[]) => any>() {
    return new DatabaseTestFactory() as Pick<DatabaseTestFactory<T>, 'setService'>;
  }

  setService(service: T) {
    this.service = service;
    return this as Pick<DatabaseTestFactory<T>, 'setRepositories'>;
  }

  setRepositories(...repositories: TRepository<T>[]) {
    this.repositories = repositories;
    return this as Pick<DatabaseTestFactory<T>, 'setInitCallback'>;
  }

  setInitCallback(initCallback: (application: INestApplication) => any) {
    this.initCallback = initCallback;
    return this as Pick<DatabaseTestFactory<T>, 'setCleanupCallback'>;
  }

  setCleanupCallback(cleanupCallback: (application: INestApplication) => Promise<void>) {
    this.cleanupCallback = cleanupCallback;
    return this as Pick<DatabaseTestFactory<T>, 'build'>;
  }

  build() {
    beforeAll(async () => {
      const moduleRef = await Test.createTestingModule({
        imports: [TypeOrmModule.forRoot(TypeOrmConfig)],
        providers: [this.service, ...RepositoryModule.register(...this.repositories)],
      }).compile();

      this.application = moduleRef.createNestApplication();

      this.initCallback(this.application);
    });

    afterEach(async () => {
      await this.cleanupCallback(this.application);
    });

    afterAll(async () => {
      const dataSource = this.application.get(DataSource);

      await dataSource.dropDatabase();
      await dataSource.destroy();
      await this.application.close();
    });
  }
}
