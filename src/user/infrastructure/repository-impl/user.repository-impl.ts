import { JoinEntity, Order, Pagination } from '@src/@shared/domain/type';
import { BaseRepositoryImpl } from '@src/@shared/infrastructure';
import { UserEntity } from '@src/user/domain/entity/user.entity';
import { UserRepository } from '@src/user/domain/repository/user.repository';

export class UserRepositoryImpl extends BaseRepositoryImpl<UserEntity> implements UserRepository {
  async findPage(
    filter: { names: string[]; roleIds: number[] },
    order: Order,
    pagination: Pagination,
  ): Promise<{ users: Omit<UserEntity, 'userRoles'>[]; totalElement: number; totalPage: number }> {
    const queryBuilder = this.createQueryBuilder('user');

    const { names, roleIds } = filter;

    if (names.length > 0) {
      queryBuilder.where('user.name in (:...names)', { names });
    }

    if (roleIds.length > 0) {
      queryBuilder.leftJoin('user.userRoles', 'userRole').andWhere('userRole.roleId in (:...roleIds)', { roleIds });
    }

    if (order.length > 0) {
      for (const o of order) {
        queryBuilder.addOrderBy(`user.${o[0]}`, o[1], o[2]);
      }
    }

    const { size, index } = pagination;

    const [users, totalElement] = await queryBuilder
      .take(size)
      .skip(size * (index - 1))
      .getManyAndCount();

    const totalPage = Math.ceil(totalElement / size);

    return { users, totalElement, totalPage };
  }

  async findOneByIdWithRoles(id: number): Promise<JoinEntity<UserEntity, 'userRoles'> | null> {
    const user = await this.createQueryBuilder('user')
      .where('user.id = :id', { id })
      .leftJoinAndSelect('user.userRoles', 'userRoles')
      .getOne();

    return user as JoinEntity<UserEntity, 'userRoles'> | null;
  }
}
