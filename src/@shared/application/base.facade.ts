export abstract class BaseFacade<T> {
  abstract execute(...args: any[]): Promise<T>;

  protected abstract generateView(...args: any): T;
}
