import { BaseClient } from '@src/@client/base.client';
import { Order } from '@src/@shared/domain/type';

// Ro = response object
export type RoleRo = { id: number; title: string };
export type RoleWhere = { all: true; ids: number[]; titles: string[] };

export abstract class CoreClient extends BaseClient {
  abstract $findManyRole<K extends keyof RoleWhere>(options: {
    where: PickEntry<RoleWhere, K>;
    order?: Order<'id' | 'title'>;
  }): Promise<RoleRo[]>;
}
