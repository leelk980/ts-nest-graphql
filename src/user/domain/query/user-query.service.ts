import { Injectable, NotFoundException } from '@nestjs/common';
import { Order, Pagination } from '@src/@shared/domain/type';
import { UserRolesDto } from '@src/user/domain/query/user-role.dto';
import { UserDto, UserPageDto } from '@src/user/domain/query/user.dto';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';
import { UserRepository } from '@src/user/domain/repository/user.repository';

@Injectable()
export class UserQueryService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly userRoleRepository: UserRoleRepository,
  ) {}

  async findUserPage(param: {
    filter: { names: string[]; roleIds: number[] };
    order: Order;
    pagination: Pagination;
  }): Promise<UserPageDto> {
    const { filter, order, pagination } = param;

    const { users, totalElement, totalPage } = await this.userRepository.findPage(filter, order, pagination);

    return { users, totalElement, totalPage };
  }

  async findOneUserById(id: number): Promise<UserDto> {
    const user = await this.userRepository.findOneById(id);
    if (!user) throw new NotFoundException('not found');

    return { user };
  }

  async findManyUserRoleByUserIds(userIds: number[]): Promise<UserRolesDto> {
    const userRoles = await this.userRoleRepository.findManyByUserIds(userIds);

    return { userRoles };
  }
}
