import { INestApplication, InternalServerErrorException } from '@nestjs/common';

let _nestApplication: INestApplication;

export const initApplication = async (options: { application: INestApplication; port?: number }) => {
  const { application, port } = options;

  _nestApplication = application;

  await application.listen(port || 3000);
};

export const getApplication = () => {
  if (!_nestApplication) {
    throw new InternalServerErrorException('not found nest app');
  }

  return _nestApplication;
};
