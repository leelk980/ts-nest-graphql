import { BaseEntity } from '@src/@shared/domain';
import { RoleTitleEnum } from '@src/core/common/enum/RoleTitleEnum';
import { Column, Entity } from 'typeorm';

@Entity('role')
export class RoleEntity extends BaseEntity {
  @Column({ type: 'enum', enum: RoleTitleEnum })
  title!: RoleTitleEnum;
}
