import { FindUserPageInput as IFindUserPageInput } from '@src/@shared/common/generated';
import { IsNumber, IsString } from 'class-validator';

export class FindUserPageInput implements IFindUserPageInput {
  @IsString({ each: true })
  names!: string[];

  @IsNumber({}, { each: true })
  roleIds!: number[];

  @IsNumber({}, { each: true })
  size!: number;

  @IsNumber()
  index!: number;
}
