import { Args, Query, Resolver } from '@nestjs/graphql';
import { FindOneUserView, FindUserPageView, IQuery } from '@src/@shared/common/generated';
import { FindOneUserFacade } from '@src/user/application/facade/find-one-user.facade';
import { FindOneUserInput } from '@src/user/application/facade/find-one-user.input';
import { FindUserPageFacade } from '@src/user/application/facade/find-user-page.facade';
import { FindUserPageInput } from '@src/user/application/facade/find-user-page.input';

type IUserQuery = Pick<IQuery, 'findUserPage' | 'findOneUser'>;

@Resolver('Query')
export class UserQueryResolver implements IUserQuery {
  constructor(
    private readonly findUserPageFacade: FindUserPageFacade,
    private readonly findOneUserFacade: FindOneUserFacade,
  ) {}

  @Query()
  async findUserPage(@Args('input') input: FindUserPageInput): Promise<FindUserPageView> {
    return this.findUserPageFacade.execute(input);
  }

  @Query()
  async findOneUser(@Args('input') input: FindOneUserInput): Promise<FindOneUserView> {
    return this.findOneUserFacade.execute(input);
  }
}
