import { faker } from '@faker-js/faker';
import { CoreClient } from '@src/@client';
import { MockTestFactory } from '@src/@shared/__test__';
import { CreateUserSaga } from '@src/user/application/saga/create-user.saga';
import { UserCommandService } from '@src/user/domain/command/user-command.service';
import { anything, when } from 'ts-mockito';

describe('CreateUserSagaTest', () => {
  let createUserSaga: CreateUserSaga;
  let userCommandService: UserCommandService;
  let coreClient: CoreClient;

  MockTestFactory.builder<typeof CreateUserSaga>()
    .setInjector(CreateUserSaga)
    .setMocks(UserCommandService, CoreClient)
    .setInitCallback((injector, [mock1, mock2]) => {
      createUserSaga = injector;
      userCommandService = mock1;
      coreClient = mock2;
    })
    .build();

  it('success', async () => {
    // given
    const userId = faker.datatype.number({ min: 1 });
    when(userCommandService.createAggregate(anything())).thenResolve(userId);
    when(coreClient.$findManyRole(anything())).thenResolve([
      { id: 1, title: 'test1' },
      { id: 2, title: 'test2' },
      { id: 3, title: 'test3' },
    ]);

    // when
    const result = await createUserSaga.execute({
      name: faker.name.fullName(),
      email: faker.internet.email(),
      summary: faker.lorem.paragraph(1),
      roleIds: [1, 2, 3],
    });

    // then
    expect(result).toEqual(userId);
  });

  it('fail to validate roleIds', async () => {
    // given
    const userId = faker.datatype.number({ min: 1 });
    when(userCommandService.createAggregate(anything())).thenResolve(userId);
    when(coreClient.$findManyRole(anything())).thenResolve([
      { id: 1, title: 'test1' },
      { id: 2, title: 'test2' },
      { id: 3, title: 'test3' },
    ]);

    // when
    const result = createUserSaga.execute({
      name: faker.name.fullName(),
      email: faker.internet.email(),
      summary: faker.lorem.paragraph(1),
      roleIds: [1, 2],
    });

    // then
    await expect(result).rejects.toThrowError();
  });
});
