import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DomainModule, TypeOrmConfig, Validator } from '@src/@config';
import '@src/@config/extension';
import { GraphQLScalars } from '@src/@config/scalar';
import { CoreModule } from '@src/core/core.module';
import { UserModule } from '@src/user/user.module';
import { ClsModule } from 'nestjs-cls';

@Module({
  imports: [
    ClsModule.register({
      global: true,
      middleware: { mount: true },
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      debug: false,
      playground: true,
      typePaths: ['./schema/**/*.graphql'],
    }),
    TypeOrmModule.forRoot(TypeOrmConfig),
    DomainModule.forRoot(UserModule, CoreModule),
  ],
  providers: [...GraphQLScalars, Validator],
})
export class AppModule implements OnApplicationBootstrap {
  private shouldRunScript = false;

  /** server on init script for dev */
  async onApplicationBootstrap() {
    if (!this.shouldRunScript || process.env.NODE_ENV === 'prod') {
      return true;
    }

    /** Do Something */
  }
}
