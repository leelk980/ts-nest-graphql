import { Query, Resolver } from '@nestjs/graphql';
import { FindAllRoleView, IQuery } from '@src/@shared/common/generated';
import { FindAllRoleFacade } from '@src/core/application/facade/find-all-role.facade';

type ICoreQuery = Pick<IQuery, 'findAllRole'>;

@Resolver()
export class CoreQueryResolver implements ICoreQuery {
  constructor(private readonly findAllRoleFacade: FindAllRoleFacade) {}

  @Query()
  async findAllRole(): Promise<FindAllRoleView> {
    return this.findAllRoleFacade.execute();
  }
}
