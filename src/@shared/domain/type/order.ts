export type Order<T = 'id'> = [T, Sort, Nulls?][];

type Sort = 'ASC' | 'DESC';

type Nulls = 'NULLS FIRST' | 'NULLS LAST';
