import { BadRequestException, Injectable } from '@nestjs/common';
import { CoreClient } from '@src/@client';
import { BaseSaga, Transaction } from '@src/@shared/application';
import { CreateUserInput } from '@src/user/application/saga/create-user.input';
import { AwsS3Adapter } from '@src/user/domain/adapter/aws-s3.adapter';
import { UserCommandService } from '@src/user/domain/command/user-command.service';

@Injectable()
export class CreateUserSaga extends BaseSaga<CreateUserInput> {
  constructor(
    private readonly userCommandService: UserCommandService,
    private readonly coreClient: CoreClient,
    private readonly awsS3Adapter: AwsS3Adapter,
  ) {
    super();
  }

  @Transaction()
  override async execute(input: CreateUserInput): Promise<number> {
    await this.validateRoleIds(input.roleIds);

    return this.createUser(input);
  }

  private async validateRoleIds(roleIds: number[]): Promise<void> {
    const roles = await this.coreClient.$findManyRole({ where: ['ids', roleIds] });

    if (roles.length !== roleIds.length) {
      throw new BadRequestException('invalid user role');
    }
  }

  private async createUser(input: CreateUserInput): Promise<number> {
    const { name, email, summary, roleIds } = input;

    return this.userCommandService.createAggregate({
      name,
      email,
      summary: summary ?? null,
      userRoles: roleIds.map((roleId) => ({ roleId })),
    });
  }
}
