import { faker } from '@faker-js/faker';
import { DatabaseTestFactory } from '@src/@shared/__test__';
import { UserCommandService } from '@src/user/domain/command/user-command.service';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';
import { UserRepository } from '@src/user/domain/repository/user.repository';

describe('UserCommandService', () => {
  let userCommandService: UserCommandService;
  let userRepository: UserRepository;
  let userRoleRepository: UserRoleRepository;

  DatabaseTestFactory.builder<typeof UserCommandService>()
    .setService(UserCommandService)
    .setRepositories(UserRepository, UserRoleRepository)
    .setInitCallback((application) => {
      userCommandService = application.get(UserCommandService);
      userRepository = application.get(UserRepository);
      userRoleRepository = application.get(UserRoleRepository);
    })
    .setCleanupCallback(async () => {
      await userRoleRepository.deleteAll();
      await userRepository.deleteAll();
    })
    .build();

  it('success', async () => {
    // when
    const userId = await userCommandService.createAggregate({
      email: faker.internet.email(),
      name: faker.name.fullName(),
      summary: faker.lorem.paragraph(1),
      userRoles: [{ roleId: faker.datatype.number({ min: 0 }) }],
    });

    // then
    expect(await userRepository.findOneById(userId)).toBeTruthy();
  });

  it('email duplication', async () => {
    // when
    const userId1 = await userCommandService.createAggregate({
      email: 'test@test.com',
      name: faker.name.fullName(),
      summary: faker.lorem.paragraph(1),
      userRoles: [{ roleId: faker.datatype.number({ min: 0 }) }],
    });

    const userId2 = userCommandService.createAggregate({
      email: 'test@test.com',
      name: faker.name.fullName(),
      summary: faker.lorem.paragraph(1),
      userRoles: [{ roleId: faker.datatype.number({ min: 0 }) }],
    });

    // then
    expect(await userRepository.findOneById(userId1)).toBeTruthy();
    await expect(userId2).rejects.toThrowError();
  });
});
