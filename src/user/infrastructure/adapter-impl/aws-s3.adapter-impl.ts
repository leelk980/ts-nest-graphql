import { BaseAdapterImpl } from '@src/@shared/infrastructure';
import { AwsS3Adapter } from '@src/user/domain/adapter/aws-s3.adapter';

export class AwsS3AdapterImpl extends BaseAdapterImpl implements AwsS3Adapter {
  getPreSignedUrl(): string {
    return '123';
  }
}
