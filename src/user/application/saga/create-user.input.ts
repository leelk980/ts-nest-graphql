import { CreateUserInput as ICreateUserInput } from '@src/@shared/common/generated';
import { ArrayMinSize, IsEmail, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateUserInput implements ICreateUserInput {
  @IsEmail()
  email!: string;

  @IsString()
  name!: string;

  @IsString()
  @IsOptional()
  summary?: Nullable<string>;

  @ArrayMinSize(1)
  @IsNumber({}, { each: true })
  roleIds!: number[];
}
