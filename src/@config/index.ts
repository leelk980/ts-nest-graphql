import { DynamicModule, ValidationPipe } from '@nestjs/common';
import { APP_PIPE } from '@nestjs/core';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as dotenv from 'dotenv';

dotenv.config({ path: `./environment/.env.${process.env.NODE_ENV}` });

export const NODE_ENV = (process.env.NODE_ENV || 'test').toUpperCase() as 'PROD' | 'DEV' | 'TEST';

export const TypeOrmConfig = {
  type: 'mysql',
  host: process.env.DATABASE_HOST,
  port: +(process.env?.DATABASE_PORT || 3306),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  synchronize: process.env.DATABASE_SYNC === 'true',
  entities: [__dirname + '../../**/*.entity{.ts,.js}'],
  logging: true,
} as TypeOrmModuleOptions;

export class DomainModule {
  public static forRoot(...domainModules: any[]): DynamicModule {
    return {
      global: true,
      module: DomainModule,
      imports: domainModules,
      exports: domainModules,
    };
  }
}

export const Validator = {
  provide: APP_PIPE,
  useValue: new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true,
  }),
};
