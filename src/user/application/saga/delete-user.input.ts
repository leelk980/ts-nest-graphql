import { DeleteUserInput as IDeleteUserInput } from '@src/@shared/common/generated';
import { IsNumber } from 'class-validator';

export class DeleteUserInput implements IDeleteUserInput {
  @IsNumber()
  id!: number;
}
