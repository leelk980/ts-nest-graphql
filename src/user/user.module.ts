import { Module } from '@nestjs/common';
import { AdapterModule, RepositoryModule } from '@src/@shared/module';
import { FindOneUserFacade } from '@src/user/application/facade/find-one-user.facade';
import { FindUserPageFacade } from '@src/user/application/facade/find-user-page.facade';
import { UserRoleLoader } from '@src/user/application/loader/user-role.loader';
import { CreateUserSaga } from '@src/user/application/saga/create-user.saga';
import { DeleteUserSaga } from '@src/user/application/saga/delete-user.saga';
import { UpdateUserSaga } from '@src/user/application/saga/update-user.saga';
import { AwsS3Adapter } from '@src/user/domain/adapter/aws-s3.adapter';
import { UserCommandService } from '@src/user/domain/command/user-command.service';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';
import { UserRepository } from '@src/user/domain/repository/user.repository';
import { UserFieldResolver } from '@src/user/interface/user-field.resolver';
import { UserMutationResolver } from '@src/user/interface/user-mutation.resolver';
import { UserQueryResolver } from '@src/user/interface/user-query.resolver';

// interface
const internals: any[] = [];
const resolvers = [UserQueryResolver, UserMutationResolver, UserFieldResolver];

// application
const facades = [FindUserPageFacade, FindOneUserFacade];
const loaders = [UserRoleLoader];
const sagas = [CreateUserSaga, UpdateUserSaga, DeleteUserSaga];

// domain
const domainServices = [UserQueryService, UserCommandService];

// infrastructure
const adapters = AdapterModule.register(AwsS3Adapter);
const repositories = RepositoryModule.register(UserRepository, UserRoleRepository);

@Module({
  providers: [
    ...internals,
    ...resolvers,
    ...facades,
    ...loaders,
    ...sagas,
    ...adapters,
    ...domainServices,
    ...repositories,
  ],
  exports: [...internals],
})
export class UserModule {}
