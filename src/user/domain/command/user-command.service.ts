import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAggregateParam } from '@src/user/domain/command/create-user.param';
import { UpdateAggregateParam } from '@src/user/domain/command/update-user.param';
import { UserRoleRepository } from '@src/user/domain/repository/user-role.repository';
import { UserRepository } from '@src/user/domain/repository/user.repository';

@Injectable()
export class UserCommandService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly userRoleRepository: UserRoleRepository,
  ) {}

  async createAggregate(param: CreateAggregateParam) {
    const { name, email, summary, userRoles } = param;

    const userId = await this.userRepository.saveOne(
      this.userRepository.createOne({
        name,
        email,
        summary,
      }),
    );

    await this.userRoleRepository.saveMany(
      this.userRoleRepository.createMany(userRoles.map(({ roleId }) => ({ userId, roleId }))),
    );

    return userId;
  }

  async updateAggregate(param: UpdateAggregateParam) {
    const { id, name, email, summary, userRoles } = param;

    const user = await this.userRepository.findOneByIdWithRoles(id);
    if (!user) throw new NotFoundException('not found');

    if (name !== undefined) {
      user.name = name;
    }

    if (email !== undefined) {
      user.email = email;
    }

    if (summary !== undefined) {
      user.summary = summary;
    }

    if (userRoles !== undefined) {
      const userId = user.id;

      await this.userRoleRepository.deleteMany(user.userRoles);
      await this.userRoleRepository.saveMany(
        this.userRoleRepository.createMany(userRoles.map(({ roleId }) => ({ userId, roleId }))),
      );
    }

    return await this.userRepository.updateOne(user);
  }

  async deleteAggregate(id: number) {
    const user = await this.userRepository.findOneByIdWithRoles(id);
    if (!user) throw new NotFoundException('not found');

    await this.userRoleRepository.deleteMany(user.userRoles);
    return this.userRepository.deleteOne(user);
  }
}
