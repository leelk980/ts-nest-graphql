
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export interface CreateUserInput {
    name: string;
    email: string;
    summary?: Nullable<string>;
    roleIds: number[];
}

export interface UpdateUserInput {
    id: number;
    name?: Nullable<string>;
    email?: Nullable<string>;
    summary?: Nullable<string>;
    roleIds?: Nullable<number[]>;
}

export interface DeleteUserInput {
    id: number;
}

export interface FindUserPageInput {
    names: string[];
    roleIds: number[];
    size: number;
    index: number;
}

export interface FindOneUserInput {
    id: number;
}

export interface BasePageView {
    pageInfo: PageInfoField;
}

export interface PageInfoField {
    totalElement: number;
    totalPage: number;
    index: number;
    size: number;
}

export interface RoleField {
    id: number;
    title: string;
}

export interface IQuery {
    findAllRole(): FindAllRoleView | Promise<FindAllRoleView>;
    findUserPage(input: FindUserPageInput): FindUserPageView | Promise<FindUserPageView>;
    findOneUser(input: FindOneUserInput): FindOneUserView | Promise<FindOneUserView>;
}

export interface FindAllRoleView {
    content: RoleField[];
    count: number;
}

export interface UserField {
    id: number;
    name: string;
    email: string;
    summary?: Nullable<string>;
    userRoles: UserRoleField[];
    createdAt: Date;
}

export interface UserRoleField {
    id: number;
    roleId: number;
    role?: Nullable<RoleField>;
}

export interface IMutation {
    createUser(input: CreateUserInput): number | Promise<number>;
    updateUser(input: UpdateUserInput): number | Promise<number>;
    deleteUser(input: DeleteUserInput): number | Promise<number>;
}

export interface FindUserPageView extends BasePageView {
    content: UserField[];
    pageInfo: PageInfoField;
}

export interface FindOneUserView {
    content: UserField;
}

type Nullable<T> = T | null;
