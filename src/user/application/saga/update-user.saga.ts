import { Injectable } from '@nestjs/common';
import { BaseSaga, Transaction } from '@src/@shared/application';
import { UpdateUserInput } from '@src/user/application/saga/update-user.input';
import { UserCommandService } from '@src/user/domain/command/user-command.service';

@Injectable()
export class UpdateUserSaga extends BaseSaga<UpdateUserInput> {
  constructor(private readonly userCommandService: UserCommandService) {
    super();
  }

  @Transaction()
  override async execute(input: UpdateUserInput) {
    const { id, name, email, summary, roleIds } = input;

    return this.userCommandService.updateAggregate({
      id,
      name: name ?? undefined,
      email: email ?? undefined,
      summary: summary,
      userRoles: roleIds?.map((roleId) => ({ roleId })) ?? undefined,
    });
  }
}
