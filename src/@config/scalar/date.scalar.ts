import { InvalidClassException } from '@nestjs/core/errors/exceptions/invalid-class.exception';
import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';

/**
 * @description
 * Timezone = UTC
 * DB에 넣을때도 UTC로 넣고 클라이언트에 전달하는 값도 UTC
 * */
@Scalar('Date')
export class DateScalar implements CustomScalar<string, Date> {
  description = 'Date custom scalar type (UTC)';

  // value from the client
  parseValue(value: any): Date {
    if (typeof value === 'string') {
      return new Date(value);
    }

    throw new InvalidClassException('invalid date type');
  }

  // value sent to the client
  serialize(value: any): string {
    if (value instanceof Date) {
      return value.toISOString() + 'hi';
    }

    throw new InvalidClassException('invalid date type');
  }

  parseLiteral(ast: ValueNode): Date {
    if (ast.kind === Kind.STRING) {
      return new Date(ast.value);
    }

    throw new InvalidClassException('invalid date type');
  }
}
