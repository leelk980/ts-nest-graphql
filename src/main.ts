import { NestFactory } from '@nestjs/core';
import { initApplication } from '@src/app.holder';
import { AppModule } from '@src/app.module';

async function bootstrap() {
  const application = await NestFactory.create(AppModule);
  const port = 3000;

  await initApplication({ application, port });

  console.log(`Server is running on ${port}`);
}

bootstrap();
