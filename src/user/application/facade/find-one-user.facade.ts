import { Injectable } from '@nestjs/common';
import { BaseFacade } from '@src/@shared/application';
import { FindOneUserView } from '@src/@shared/common/generated';
import { FindOneUserInput } from '@src/user/application/facade/find-one-user.input';
import { UserQueryService } from '@src/user/domain/query/user-query.service';
import { UserDto } from '@src/user/domain/query/user.dto';

@Injectable()
export class FindOneUserFacade extends BaseFacade<FindOneUserView> {
  constructor(private readonly userQueryService: UserQueryService) {
    super();
  }

  override async execute(input: FindOneUserInput): Promise<FindOneUserView> {
    const { id } = input;
    const userDto = await this.userQueryService.findOneUserById(id);

    return this.generateView(userDto);
  }

  protected override generateView(userDto: UserDto): FindOneUserView {
    const { id, name, email, summary, createdAt } = userDto.user;

    return {
      content: {
        id,
        name,
        email,
        summary,
        createdAt,
        userRoles: [],
      },
    };
  }
}
