import { BaseEntity } from '@src/@shared/domain';
import { UserEntity } from '@src/user/domain/entity/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('user_role')
export class UserRoleEntity extends BaseEntity {
  /** Foreign keys */
  @Column({ type: 'int' })
  userId!: number;

  @Column({ type: 'int' })
  roleId!: number;

  /** Relations */
  @ManyToOne(() => UserEntity, (user) => user.userRoles)
  @JoinColumn()
  user?: UserEntity;
}
