type RoleVo = {
  id: number;
  title: string;
};

export type RoleDto = {
  role: RoleVo[];
};

export type RolesDto = {
  roles: RoleVo[];
};
