import { BaseRepository, Repository } from '@src/@shared/domain';
import { RoleEntity } from '@src/core/domain/entity/role.entity';
import { RoleRepositoryImpl } from '@src/core/infrastructure/repository-impl/role.repository-impl';

@Repository(RoleEntity, RoleRepositoryImpl)
export abstract class RoleRepository extends BaseRepository<RoleEntity> {}
