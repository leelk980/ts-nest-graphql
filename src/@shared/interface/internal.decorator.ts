import { SetMetadata } from '@nestjs/common';
import { BaseClient } from '@src/@client/base.client';

export const INTERNAL = 'CLIENT_IMPL';

export function Internal<T extends BaseClient>(client: abstract new (...args: any[]) => T): ClassDecorator {
  return SetMetadata(INTERNAL, client);
}
