import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import * as madge from 'madge';
import { join } from 'path';

const type = process.argv.find((each) => each.startsWith('type='))?.split('type=')[1];

if (!type) {
  console.error('[ERROR]: Cannot find script type');
  process.exit(1);
}

if (['all', 'madge'].includes(type)) {
  madge('dist/src/main.js').then((res) => {
    console.log('============================ madge ============================');
    console.log(res.circular());
    console.log('===============================================================');
  });
}

if (['all', 'schema'].includes(type)) {
  const definitionsFactory = new GraphQLDefinitionsFactory();

  definitionsFactory.generate({
    typePaths: [`./schema/**/*.graphql`],
    path: join(process.cwd(), `./src/@shared/common/generated.ts`),
  });
}
