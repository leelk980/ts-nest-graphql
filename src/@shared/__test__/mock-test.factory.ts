import { BaseClient } from '@src/@client/base.client';
import { ADAPTER_IMPL, BaseAdapter } from '@src/@shared/application';
import { BaseRepository, REPOSITORY_IMPL } from '@src/@shared/domain';
import { INTERNAL } from '@src/@shared/interface';
import { instance, mock, reset } from 'ts-mockito';

jest.setTimeout(30000);

type Mock<T extends new (...args: any[]) => any> = abstract new (...args: any) => UnwrapArray<ConstructorParameters<T>>;

type InitCallback<T extends new (...args: any[]) => any> = (
  injector: InstanceType<T>,
  mocks: ConstructorParameters<T>,
) => void;

/** @description
 * This factory is for application layer test
 * */
export class MockTestFactory<T extends new (...args: any[]) => any> {
  private injector!: T;
  private mocks!: Mock<T>[];
  private initCallback!: InitCallback<T>;

  private constructor() {
    /***/
  }

  static builder<T extends new (...args: any[]) => any>() {
    return new MockTestFactory() as Pick<MockTestFactory<T>, 'setInjector'>;
  }

  setInjector(injector: T) {
    this.injector = injector;
    return this as Pick<MockTestFactory<T>, 'setMocks'>;
  }

  setMocks(...mocks: Mock<T>[]) {
    this.mocks = [];
    for (const mock of mocks) {
      if (this.isBaseRepository(mock)) {
        this.mocks.push(Reflect.getMetadata(REPOSITORY_IMPL, mock)[1]);
      }

      if (this.isBaseAdapter(mock)) {
        this.mocks.push(Reflect.getMetadata(ADAPTER_IMPL, mock));
      }

      if (this.isBaseClient(mock)) {
        this.mocks.push(Reflect.getMetadata(INTERNAL, mock));
      }

      this.mocks.push(mock);
    }

    return this as Pick<MockTestFactory<T>, 'setInitCallback'>;
  }

  setInitCallback(initCallback: InitCallback<T>) {
    this.initCallback = initCallback;
    return this as Pick<MockTestFactory<T>, 'build'>;
  }

  build() {
    const mockInstances = this.mocks.map((m) => mock(m));
    const injectorInstance = new this.injector(...mockInstances.map((mi) => instance(mi)));

    beforeAll(() => {
      for (const mi of mockInstances) reset(mi);
    });

    afterEach(() => {
      for (const mi of mockInstances) reset(mi);
    });

    this.initCallback(injectorInstance, mockInstances as ConstructorParameters<T>);
  }

  private isBaseRepository(mock: Mock<T>): boolean {
    return mock.prototype instanceof BaseRepository;
  }

  private isBaseAdapter(mock: Mock<T>): boolean {
    return mock.prototype instanceof BaseAdapter;
  }

  private isBaseClient(mock: Mock<T>): boolean {
    return mock.prototype instanceof BaseClient;
  }
}
