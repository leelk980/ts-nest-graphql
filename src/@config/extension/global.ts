declare global {
  type Nullable<T> = T | null;

  type UnwrapArray<T> = T extends Array<infer R> ? R : never;

  type UnwrapPromise<T> = T extends Promise<infer R> ? R : never;

  type PickEntry<T extends Record<any, any>, K extends keyof T> = [K, T[K]];
}

export {};
